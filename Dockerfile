FROM python:3
LABEL Name=eegras-fuck-spectrum Version=0.0.1

WORKDIR ['autoexpenses']
COPY requirements.txt .
RUN pip install --no-cache-dir -r ./requirements.txt
COPY ./src .

ARG HOSTNAME
ARG USERNAME
ARG PASSWORD

ENV HOSTNAME $HOSTNAME
ENV USERNAME $USERNAME
ENV PASSWORD $PASSWORD

CMD ["python",  "./main.py"]
