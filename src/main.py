from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.firefox.options import Options
import time
import sys
import os


SPECTRUM_USERNAME = os.environ['USERNAME']
SPECTRUM_PASSWORD = os.environ['PASSWORD']

firefox_options = webdriver.FirefoxOptions()
firefox_options.headless = False

firefox_options.set_preference("browser.download.folderList", 2)
firefox_options.set_preference("browser.download.manager.showWhenStarting", False)
firefox_options.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf")
firefox_options.set_preference("browser.download.dir", "/downloads")
firefox_options.set_preference("pdfjs.disabled", True)

driver = webdriver.Remote(command_executor=f"http://{os.environ['HOSTNAME']}",
                          options=firefox_options)


def waitUntil(selector_type, selector_name, delay=20):
    try:
        myElem = WebDriverWait(driver, delay).until(EC.presence_of_element_located((selector_type, selector_name)))
        return myElem
    except TimeoutException:
        raise


driver.delete_all_cookies()
driver.get("http://www.spectrum.net")

waitUntil(By.ID, "login-button").click()

elem = waitUntil(By.ID, "cc-username")
elem.clear()
elem.send_keys(SPECTRUM_USERNAME)

elem = waitUntil(By.ID, "cc-user-password")
elem.clear()
elem.send_keys(SPECTRUM_PASSWORD)

time.sleep(2)

waitUntil(By.CLASS_NAME, "dialog_button").click()

waitUntil(By.ID, "homepage-core-cta-make-a-payment-button")

driver.get("http://www.spectrum.net/billing")

elem = waitUntil(By.ID, "statements-select", delay=40)

time.sleep(20)

waitUntil(By.CSS_SELECTOR,
          ".view-printable-statement.kite-link.ngk-link.kite-typography.kite-standalone-link",
          delay=20).click()

time.sleep(600)

driver.close()
driver.quit()
